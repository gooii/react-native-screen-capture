# react-native-screen-capture

**`Screen Capture`** allows you to protect screens in your app from being captured or recorded.

## Installation

```sh
npm i react-native-screen-capture
```
## Preview
![](preview.gif)

## Usage

```js
import { allowScreenshot } from 'react-native-screen-capture';

// disable screenshots
allowScreenshot(false);

// enable screenshots
allowScreenshot(true);

## License

MIT
