/*
 * Shamelessly based on https://github.com/JayantBadlani/ScreenShield
 *
 * This is iOS only so have removed some surplus code
 *
 * Also attempts to _unprotect_ the view if false is passed to the bridging module method
 */
import UIKit
import SwiftUI

// expose this to Objective C and RN's bridge module
@objc(ScreenCapture)
class ScreenCapture: NSObject {
    // let React Native know if your module needs to be initialized on the main thread, before any JavaScript code executes
    @objc static func requiresMainQueueSetup() -> Bool { return true }
    
    // expose functions defined in RN bridging module
    @objc public func allowScreenshot(_ allow: Bool) -> Void {
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            if let view = RCTPresentedViewController()?.view {
                if allow == false {
                    ScreenShield.shared.enable(view: view)
                }
                else {
                    ScreenShield.shared.remove(view: view)
                }
            }
        })
    }
}

// MARK: UIKit
class ScreenShield: NSObject {
    public static let shared = ScreenShield()
    private var blurView: UIVisualEffectView?
    private var recordingObservation: NSKeyValueObservation?
    private var secureTextField: UITextField?
    private var superlayer: CALayer?
               
    public func enable(view: UIView) {
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            view.setScreenCaptureProtection(&self.superlayer, secureField: &self.secureTextField)
        })
    }

    public func remove(view: UIView) {
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            view.rmvScreenCaptureProtection(&self.superlayer, secureField: &self.secureTextField)
        })
    }

    public func protectFromScreenRecording() {
        recordingObservation =  UIScreen.main.observe(\UIScreen.isCaptured, options: [.new]) { [weak self] screen, change in
            let isRecording = change.newValue ?? false
            
            if isRecording {
                self?.addBlurView()
            } else {
                self?.removeBlurView()
            }
        }
    }
    
    private func addBlurView() {
        let blurEffect = UIBlurEffect(style: .regular)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = UIScreen.main.bounds
        
        // Add a label to the blur view
        let label = UILabel()
        label.text = "Screen recording not allowed"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        blurView.contentView.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: blurView.contentView.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: blurView.contentView.centerYAnchor)
        ])
        
        self.blurView = blurView
        UIApplication.shared.windows.first { $0.isKeyWindow }?.addSubview(blurView)
    }
    
    private func removeBlurView() {
        blurView?.removeFromSuperview()
        blurView = nil
    }
}

extension UIView {
    private struct Constants {
        static var secureTextFieldTag: Int { 54321 }
    }
    
    func setScreenCaptureProtection(_ superlayer: inout CALayer?, secureField: inout UITextField?) {
        if secureField != nil {
            // bomb out if this view already has the secure text field we're looking
            // for
            return
        }
        // check this view exists in the hierarchy (has a superview)
        guard superview != nil else {
            // it doesn't so enumerate each subview...
            for subview in subviews {
                // recursively invoke setScreenCaptureProtection() until
                // we find a view which is attached (has a superview)
                subview.setScreenCaptureProtection(&superlayer, secureField: &secureField)
            }
            return
        }
        // log
        print("Creating UITextField. Original layer::", layer)
        // store ref to original super layer as we're about to modify it
        superlayer = layer.superlayer
        // this view has a superview and has no secure text field entry so
        // create one ...
        let sf = UITextField()
        //
        sf.translatesAutoresizingMaskIntoConstraints = false
        // make it transparent; not really needed as we insert
        // it as the first subview of THIS view so it'll be at
        // the back anyway and therefore not visible
        sf.backgroundColor = UIColor.clear
        // tag it so we can find it later
        sf.tag = Constants.secureTextFieldTag
        // the magic bit
        sf.isSecureTextEntry = true
        // should allow gestures to pass through
        sf.isUserInteractionEnabled = false
        // add the field to this view at the back (not visible)
        insertSubview(sf, at: 0)
        // whilst the field is at the back of the view stack
        // extract it's layer and present it as the top most
        // layer of this view
        layer.superlayer?.addSublayer(sf.layer)
        // here's the trick - we now have to add this view's
        // layer as the last sublayer in our text field
        //
        // remember that the text field VIEW is still at the
        // back of the stack we have just brought it's layer
        // forward
        //
        // by adding this view's layer to the text field, we
        // are essentially protecting it whilst still making
        // the app visible and interactable
        sf.layer.sublayers?.last?.addSublayer(layer)
        // set secureField reference
        secureField = sf
    }
    
    // doesn't work at the moment
    func rmvScreenCaptureProtection(_ superlayer: inout CALayer?, secureField: inout UITextField?) {
        // bomb out if there's no secureField to remove...
        guard secureField != nil else {
            print("Secure UITextField not initialised - nothing to do...")
            return
        }
        // check this view exists in the hierarchy (has a superview)
        guard superview != nil else {
            // it doesn't so enumerate each subview...
            for subview in subviews {
                // recursively invoke rmvScreenCaptureProtection() until we
                // find a view with a superview
                subview.rmvScreenCaptureProtection(&superlayer, secureField: &secureField)
            }
            return
        }
        // check we have the original superlayer
        guard superlayer != nil else {
            print("No superlayer reference provided. Cannot restore.")
            return
        }
        // remove this layer from the secure field layers
        layer.removeFromSuperlayer()
        // remove the secure field view including all layers
        secureField?.removeFromSuperview()
        // restore layer back to it's original position
        superlayer?.addSublayer(layer)
        // clear reference
        secureField = nil
        // clear superlayer reference
        superlayer = nil
    }
}

// MARK:  SwiftUI
public struct ProtectScreenshot: ViewModifier {
    public func body(content: Content) -> some View {
        ScreenshotProtectView { content }
    }
}

public extension View {
    func protectScreenshot() -> some View {
        modifier(ProtectScreenshot())
    }
}

struct ScreenshotProtectView<Content: View>: UIViewControllerRepresentable {
    typealias UIViewControllerType = ScreenshotProtectingHostingViewController<Content>
    
    private let content: () -> Content
    
    init(@ViewBuilder content: @escaping () -> Content) {
        self.content = content
    }
    
    func makeUIViewController(context: Context) -> UIViewControllerType {
        ScreenshotProtectingHostingViewController(content: content)
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {}
}

final class ScreenshotProtectingHostingViewController<Content: View>: UIViewController {
    private let content: () -> Content
    private let wrapperView = ScreenshotProtectingView()
    
    init(@ViewBuilder content: @escaping () -> Content) {
        self.content = content
        super.init(nibName: nil, bundle: nil)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        view.addSubview(wrapperView)
        wrapperView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            wrapperView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            wrapperView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            wrapperView.topAnchor.constraint(equalTo: view.topAnchor),
            wrapperView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        let hostVC = UIHostingController(rootView: content())
        hostVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        addChild(hostVC)
        wrapperView.setup(contentView: hostVC.view)
        hostVC.didMove(toParent: self)
    }
}

public final class ScreenshotProtectingView: UIView {
    private var contentView: UIView?
    private let textField = UITextField()
    private lazy var secureContainer: UIView? = try? getSecureContainer(from: textField)
    
    public init(contentView: UIView? = nil) {
        self.contentView = contentView
        super.init(frame: .zero)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        textField.backgroundColor = .clear
        textField.isUserInteractionEnabled = false
        textField.isSecureTextEntry = true
        
        guard let container = secureContainer else { return }
        
        addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            container.leadingAnchor.constraint(equalTo: leadingAnchor),
            container.trailingAnchor.constraint(equalTo: trailingAnchor),
            container.topAnchor.constraint(equalTo: topAnchor),
            container.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        guard let contentView = contentView else { return }
        setup(contentView: contentView)
    }
    
    public func setup(contentView: UIView) {
        self.contentView?.removeFromSuperview()
        self.contentView = contentView
        
        guard let container = secureContainer else { return }
        
        container.addSubview(contentView)
        container.isUserInteractionEnabled = isUserInteractionEnabled
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: container.bottomAnchor)
        bottomConstraint.priority = .required - 1
        
        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: container.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: container.trailingAnchor),
            contentView.topAnchor.constraint(equalTo: container.topAnchor),
            bottomConstraint
        ])
    }
    
    func getSecureContainer(from view: UIView) throws -> UIView {
        let containerName: String
        
        if #available(iOS 15, *) {
            containerName = "_UITextLayoutCanvasView"
        } else if #available(iOS 14, *) {
            containerName = "_UITextFieldCanvasView"
        } else {
            let currentIOSVersion = (UIDevice.current.systemVersion as NSString).floatValue
            throw NSError(domain: "YourDomain", code: -1, userInfo: ["UnsupportedVersion": currentIOSVersion])
        }
        
        let containers = view.subviews.filter { type(of: $0).description() == containerName }
        
        guard let container = containers.first else {
            throw NSError(domain: "YourDomain", code: -1, userInfo: ["ContainerNotFound": containerName])
        }
        
        return container
    }
}
