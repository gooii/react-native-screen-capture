#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(ScreenCapture, NSObject)

RCT_EXTERN_METHOD(allowScreenshot: (BOOL)allow)

@end
